# Neo4jcrime



## Getting started
```
# Activate your virtual environment
# Co    py the sample environment variables file to .env
(.venv) % cp .env.sample .env

# Update .env with your Neo4j credentials

# Install and run with poetry
peotry use C:\Users\Garyb\AppData\Local\Programs\Python\Python312\python.exe
poetry install
poetry run python main.py
```



# Real steps


Area of interest
sa4	sa4name
305	Brisbane Inner City
302	Brisbane - North
301	Brisbane - East
304	Brisbane - West
303	Brisbane - South



1. Create offensives grouped by meshblock.
The crime data was download from a public url. And placed into offences.csv these were aggregated per meshblock per day for the chosen aoi and placed in a files called offeces_aggreated.csv
```
CREATE CONSTRAINT OffenceType
FOR (ot:OffenceType) REQUIRE ot.name IS UNIQUE
```
```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/offences_agg.tsv' as offence FIELDTERMINATOR '\t'
CREATE (cs:CrimeSuperSet { count: offence.count, date: offence.monthyear, meshblock: offence.meshblock, type:offence.Type })
MERGE (cs)-[:LOCATED_IN]->(mb:MeshBlock { MB_CODE21: offence.meshblock})

```

```
match (cs:CrimeSet)
match (m:MeshBlock)
MERGE (cs)-[:LOCATED_IN]->(mb:MeshBlock { MB_CODE21: cs.meshblock})
```

<!-- ```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/offences_agg.tsv' as offence FIELDTERMINATOR '\t'
CREATE (cs:CrimeSet { count: offence.count, date: offence.monthyear, meshblock: offence.meshblock })
MERGE(ct:OffenceType {name: offence.Type})
MERGE (cs)-[:APPEARS_IN]->(mb:MeshBlock { MB_CODE21: offence.meshblock})
MERGE (cs)-[hct:HAS_OFFENCE_TYPE]->(ct)
``` -->


2. Created data using used uploadMeshBlocks.ipynb to clean data
Loaded the meshblocks with a weighted mean centre.
Loaded mesh blocks using uploadMeshBlocks./py
The script involves placing the shapefile data into a geodataframe using geopandas and filtering them for the chosen area of interest then converting the individual meshblocks as records in neo4j with the geographical data formatted as Well-known text (WKT).
-- Created from this are exports in the form of a dump file and csv files.
```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gdfBris.csv' as mesh 
wITH *
SKIP 0 LIMIT 30000
MERGE(mb:MeshBlock {MB_CODE21: mesh.MB_CODE21})
SET mb += mesh
```

And create the point
```
MATCH (n:MeshBlock) 
set n.center = point({x: toFloat(n.longitude), y: toFloat(n.latitude)})
```

To test you can get the distance between 2
```
MATCH (n:MeshBlock)
 with n order by rand() limit 1
MATCH(b:MeshBlock)
with n, b order by rand() limit 1
return n.SA2_NAME21, b.SA2_NAME21, n.center.x, b.center,  point.distance(point({longitude: n.center.x, latitude: n.center.y}), point({longitude: b.center.x, latitude: b.center.y})) / 1000 as km
```


3. Upload sa1s with G01 information (population), created from geopackage layer. used clean_sa1_G01.ipynb to clean data
```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gdfBris_sa1.csv' as saonegzerocsv 
wITH *
MERGE(saonegzero:SA1 {SA1_CODE_2021: saonegzerocsv.SA1_CODE_2021})
SET saonegzero += saonegzerocsv
```


Also add G02 information (selected means

```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gdfBris_sa1_go2.csv' as sa1g1s
wITH *
MERGE(saonegone:SA1 {SA1_CODE_2021: sa1g1s.SA1_CODE_2021})
SET saonegone += sa1g1s
```

And create the point
```
MATCH (n:SA1) 
set n.center = point({x: toFloat(n.longitude), y: toFloat(n.latitude)})
```

Create the relationship
```
MATCH (n:SA1)
MATCH(mb:MeshBlock { SA1_CODE21: n.SA1_CODE_2021})
MERGE(mb)-[i:BELONGS_TO_SA1]->(n)
```



Police stations and police beats
https://www.data.qld.gov.au/dataset/qps-police-stations/resource/b0aa480e-f023-4bbf-9c54-3a8036edfb14
https://www.data.qld.gov.au/dataset/qps-police-beats/resource/44006aec-49e3-4872-92c7-38a8a126e510
Need to add their Meshblock location in QGIS.

https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/police_stations.csv
https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/police_beats.csv


LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/police_beats.csv' as stations
wITH *
MERGE(station:PoliceStation {SA1_CODE: stations.SA1_CODE_2})
SET station += stations


MATCH (n:PoliceStation)
MATCH(mb:MeshBlock { MB_CODE21: n.MB_CODE21})
MERGE(n)-[i:LOCATED_IN]->(mb)


```
MATCH (n:PoliceStation) 
set n.center = point({x: toFloat(n.longitude), y: toFloat(n.latitude)})
```

MATCH(n:PoliceStation)-[i:LOCATED_IN]->(mb:MeshBlock)-[b:BELONGS_TO_SA1]->(sa:SA1)
return n, i, mb, b ,sa

36343200000
---
Now load the ancillaries, do this three times, changing the attribute where appropriate
---

trains.csv TrainStation
parks_points_with_mb.csv Park
bus_with_mb.csv BusStation

LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/parks_points_with_mb.csv' as row
WITH *
MERGE (:Amenity {
    AmenityID: row.OBJECTID, 
    AmenityType: "Park",
    MB_CODE21: row.MB_CODE21, 
    center: point({latitude: toFloat(row.latitude), longitude: toFloat(row.longitude)}) 
})

LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/parks_points_with_mb.csv' AS row
MERGE (a:Amenity {AmenityID: row.OBJECTID})
SET a += row

```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/trains.csv' as row
WITH *
MERGE (:Amenity {
    AmenityID: row.name, 
    AmenityType: "TrainStation",
    MB_CODE21: row.MB_CODE21, 
    center: point({latitude: toFloat(row.latitude), longitude: toFloat(row.longitude)}) 
})
```
```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/trains.csv' AS row
MERGE (a:Amenity {AmenityID: row.name})
SET a += row
```

MATCH (n:Amenity)
MATCH(mb:MeshBlock { MB_CODE21: n.MB_CODE21})
MERGE(n)-[i:LOCATED_IN]->(mb)


## Fun queries

<!-- MATCH (a:Amenity), (m:Meshblock)
WHERE distance(a.location, m.geometry) < 500 // Adjust distance threshold as needed
CREATE (a)-[:NEAR]->(m) -->


MATCH (a:Amenity)-[:APPEARS_IN]->(mb:Meshblock)<-[:LOCATED_IN]-(ps:PoliceStation)
RETURN a.AmenityID, a.AmenityType, mb.MB_CODE21, ps.PoliceStationID

CREATE SPATIAL INDEX amenity_spatial_index FOR (a:Amenity) ON a.location
# Notes
<!-- 
1. Loaded post codes and extracted.
https://www.matthewproctor.com/australian_postcodes#downloadlinks. Postcodes downloaded and placed into s3, then imported using  
https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/australian_postcodes.csv
``` LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/offences.csv' as row Return row limit 1```

Data extracted then
```sql
WITH location
MERGE (location:Suburb {locationId: locationId})
SET location += {
  sysName: name,
  geom: point({latitude: toFloat(lat), longitude: toFloat(long)}),
  import: '$date'
}
```

Check the data
```
MATCH (s:Suburb)-[:IS_LOCATED_IN]-(b:State)
return s, b
```

2. Loaded mesh blocks using uploadMeshBlocks./py

3. -->


<!-- 1. 
 Loaded Brisbane Crimes for the past 3 months for all suburbs in.
Using the postcode data, the areas 
=offences.csv
https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/offences.csv
```SQL
CALL {
    WITH 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/offences.csv' AS uri
    LOAD CSV WITH HEADERS FROM uri AS row
    WITH row
    CREATE (incident:CrimeIncident)
    SET incident.id = row.Type + "_" + row.Date + "_" + row.ABS_Meshblock,
        incident.type = row.Type,
        incident.date = apoc.date.parse(row.Date, "s", "yyyy-MM-dd HH:mm:ss"),
        incident.postcode = row.Postcode,
        incident.areaOfInterest = row.Area_of_Interest
} IN TRANSACTIONS OF 100 ROWS

With incident
MATCH (mb:MeshBlocks {code: incident.ABS_Meshblock})
MERGE (incident)-[:OCCURRED_IN]->(mb)
With incident
MATCH (s:Suburb {postcode: incident.postcode})
MERGE (incident)-[:OCCURRED_IN]->(s)
``` -->




<!-- 4. population and sa2 polygons, cleaned and filtered for brisbane, https://www.abs.gov.au/statistics/people/population/regional-population/latest-release#data-download

// https://www.abs.gov.au/statistics/people/population/regional-population/latest-release#data-download
Then linked the below to the sa2 polygons from https://www.abs.gov.au/census/find-census-data/datapacks?release=2021&product=GCP&geography=SA2&header=S or https://www.abs.gov.au/census/find-census-data/geopackages for polygons
4. Education G01 - Selected person characteristics by sex
5. Income - G02 - Selected medians and averages
6. more education g15 - Education 2021Census_G15_QLD_SA2 -->

`MATCH (n:MeshBlocks) 
set n.center = point({x: toFloat(n.longitude), y: toFloat(n.latitude)})`

## junk
Both from https://www.abs.gov.au/census/find-census-data/geopackages?release=2021&geography=QLD&gda=GDA2020&table=G01

4. https://www.abs.gov.au/statistics/people/population/regional-population/latest-release#data-download%20https://datapacks.censusdata.abs.gov.au/geopackages/ 


5. https://www.abs.gov.au/census/find-census-data/geopackages?release=2021&geography=QLD&gda=GDA2020&topic=EI - selected medians

6. Life expectancy https://www.abs.gov.au/statistics/people/population/life-expectancy/latest-release


# gameplan 30/7
download all meshblocks uploaded.
upload crime
upload sa2 G01 and join on G01
locally
count each crime for the past 6 months for each sa2
join data to offences from meshblocks, combine spreadsheets of meshblocks and do dataframe join and resave
done, neo 4j done.


#gamplan 14/08

Upload
https://www.abs.gov.au/census/find-census-data/geopackages?release=2021&geography=QLD&gda=GDA2020&table=G01

Then everything else from thesis suggestion

city centres, night clubs, train stations.
enough data to fill out HDI. polygons
GIS queries.




GEMINI
1. Contextualized Spatial Analysis

Identify crime hotspots near specific amenities:

Cypher
MATCH (m:Meshblock)-[:APPEARS_IN]->(c:CrimeSet)
WHERE c.Count > 100 // Adjust threshold as needed
WITH m, sum(c.Count) AS totalCrimes
MATCH (m)-[:NEAR]->(a:Amenity)
WHERE a.AmenityType = "Nightclub" // Change to other amenity types
RETURN m.MB_CODE21, m.sa2_name21, a.AmenityType, a.AmenityID, totalCrimes
ORDER BY totalCrimes DESC
Use code with caution.

Analyze the impact of socioeconomic factors on crime:

Cypher
MATCH (m:Meshblock)-[:BELONGS_TO_SA1]->(sa1:SA1)
WHERE sa1.MedianIncome < 50000 // Adjust threshold as needed
WITH m, sa1
MATCH (m)<-[:APPEARS_IN]-(c:CrimeSet)
RETURN sa1.SA1_CODE21, sa1.MedianIncome, sum(c.Count) AS totalCrimes, collect(c.OffenceType) AS offenceTypes
ORDER BY totalCrimes DESC
Use code with caution.

Compare crime patterns in different contexts:

Cypher
// Find meshblocks with similar crime profiles but different SA2s
MATCH (m1:Meshblock)<-[:APPEARS_IN]-(c1:CrimeSet)
WITH m1, collect(c1.OffenceTypeID) AS m1OffenceTypes, sum(c1.Count) AS m1TotalCrimes
MATCH (m2:Meshblock)<-[:APPEARS_IN]-(c2:CrimeSet)
WHERE m1.MB_CODE21 <> m2.MB_CODE21 AND m1.SA2_CODE21 <> m2.SA2_CODE21
WITH m1, m1OffenceTypes, m1TotalCrimes, m2, collect(c2.OffenceTypeID) AS m2OffenceTypes, sum(c2.Count) AS m2TotalCrimes
WHERE m1OffenceTypes = m2OffenceTypes AND abs(m1TotalCrimes - m2TotalCrimes) < 10 // Adjust similarity thresholds
RETURN m1.MB_CODE21, m1.SA2_NAME21, m2.MB_CODE21, m2.SA2_NAME21, m1OffenceTypes, m1TotalCrimes, m2TotalCrimes
Use code with caution.



2. Flexibility and Adaptability

Add new node types and relationships:

If you gather data on CCTV camera locations, create a CCTVCamera node type and a HAS_CCTV relationship to connect it to Meshblock nodes.
Showcase how you can easily modify your graph to incorporate this new data and explore its relationship to crime.
Modify existing queries:

Update your queries to include the new data and relationships. For example, you could analyze crime rates in meshblocks with and without CCTV cameras.
3. Expressiveness of Queries

Formulate complex spatial and multi-hop queries:

Find meshblocks with high crime rates that are within walking distance of a park but not adjacent to a major road.
Identify areas with a high concentration of specific amenities (e.g., bars and nightclubs) and analyze their correlation with certain types of crime.
Compare Cypher and SQL:

If possible, implement equivalent queries in both Cypher and SQL (for a traditional geodatabase) to highlight the conciseness and readability of Cypher.
4. Ease of Visualization

Use Neo4j Bloom or other graph visualization tools:

Create interactive visualizations that showcase crime hotspots, their relationships to amenities and socioeconomic factors, and any patterns identified in your analysis.
Demonstrate how these visualizations help in understanding the local context of crime and communicating your findings effectively.
General Analysis Strategies:

Community Detection: Use graph algorithms to identify clusters of meshblocks with similar crime profiles.
Spatial Pattern Analysis: Analyze the spatial distribution of crime in relation to different amenities and socioeconomic factors.
Pathfinding: Find the shortest paths between crime hotspots and police stations or other relevant locations.
Statistical Analysis: Calculate crime statistics (e.g., mean, standard deviation) for different areas and time periods.
By combining these queries and analysis strategies, you can effectively showcase the key benefits of your knowledge graph for understanding and addressing crime in Brisbane.


// Matt and/or  notes.

See how fast they are without indexing


// query intrinsic properties, break it down
// database, table reads,
Theoritcal faster, say it theoritcally

1. QUery that is simple, that demonstrates valuobale results
2. Ask alex to make the query, best query, most efficient
3. Measure them, time, syscalls, memory usage (do the query for multiple sizes of data) //data reads, memory, size of data, find out more things it can track.


a weird word exercise, where run it, validate it and hope it works.
Brute forcing it.

or theoritially come up with a good one.    

```mermaid
graph LR
    subgraph "Nodes"
        MB[MeshBlock]
        OF[Offences]
        CS[CrimeSet]
        PS[PoliceStation]
        SA1[SA1]
        P[Park]
        T[Transport]
    end
    subgraph "Relationships"
        MB -->|"BELONGS_TO_SA1"| SA1
        CS -->|"LOCATED_IN"| MB
        OF -->|"LOCATED_IN"| MB
        PS -->|"LOCATED_IN"| MB
        P -->|"LOCATED_IN"| MB 
        T -->|"LOCATED_IN"| MB 
    end
    
```


Need to 


TODO, create postgres database


2.

figure out how to add near relationships to the meshblocks
so you can do this

MATCH (c1:CrimeSuperSet)-[:NEAR*..5]->(c2:CrimeSuperSet) // Find crimes connected by up to 5 "near" relationships
RETURN c1, c2


add in the indiviudal crimes, set the followed_by link

MATCH (c1:CrimeSuperSet), (c2:CrimeSuperSet)
WHERE c1.Date < c2.Date 
AND duration.between(c1.Date,c2.Date).minutes <= 60*24*7 // Adjust time frame as needed
CREATE (c1)-[:FOLLOWED_BY]->(c2)

so you can do this

MATCH (c1:CrimeSuperSet), (c2:CrimeSuperSet)
WHERE c1.Date < c2.Date 
AND duration.between(c1.Date,c2.Date).minutes <= 60*24*7 // Adjust time frame as needed
CREATE (c1)-[:FOLLOWED_BY]->(c2)



ok final game plan, queries, timing engine, graphs, write up.