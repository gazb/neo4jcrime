MATCH (n:MeshBlock)<-[:LOCATED_IN]-(n1:Offence)
WITH n, n1
MATCH (n3:SA1) 
WHERE point.withinBBox(n.center, n3.bottomLeft, n3.topRight)
WITH n3.SA1_NAME_2021 AS name, count(n1) AS numberOfOffences 
RETURN name, numberOfOffences 
ORDER BY numberOfOffences DESC

MATCH (n:MeshBlock)<-[:LOCATED_IN]-(n1:Offence)
WITH n,n1 
MATCH (n3:SA1) 
WITH n,n1,n3 
WHERE point.withinBBox(n.location,n3.bottomLeft,n3.topRight)
WITH n3.Median_age_persons as medianAge, count(n1) as numberOfOffences
WITH medianAge, max(numberOfOffences) as maxOffences
RETURN medianAge,maxOffences
ORDER BY maxOffences DESC LIMIT 1



MATCH (n:Meshblock)<-[:LOCATED_IN]-(n1:Offence) 
WITH n,n1 
MATCH (n3:Sa1) 
WITH n,n1,n3 
WHERE point.withinBBox(n.center,n3.bottomLeft,n3.topRight)
WITH n3.Average_household_size as householdSize, count(n1) as numberOfOffences
WITH householdSize,max(numberOfOffences) as maxOffences
RETURN householdSize,maxOffences
ORDER BY maxOffences DESC LIMIT 5

MATCH (o:Offence)-[:LOCATED_IN]->(mb:MeshBlock)
WITH mb, o
MATCH (sa:SA1) 
WHERE point.withinBBox(mb.center, sa.bottomLeft, sa.topRight)
WITH sa.Median_age_persons AS medianAge, count(o) AS numberOfOffences 
RETURN medianAge, numberOfOffences 
ORDER BY numberOfOffences DESC, medianAge DESC

MATCH (mb1:MeshBlock)-[:NEAR]->(mb2:MeshBlock)
WITH mb1, mb2
MATCH (mb1)<-[:LOCATED_IN]-(c1:CrimeSuperSet), (mb2)<-[:LOCATED_IN]-(c2:CrimeSuperSet)
WITH mb1, mb2, abs(sum(c1.count) - sum(c2.count)) AS crime_count_difference
WHERE crime_count_difference < 5
RETURN mb1.MB_CODE21, mb2.MB_CODE21, crime_count_difference



MATCH (mb:MeshBlock {MB_CODE21: '30119020000'})-[:NEAR*1..10]->(neighbor)
WITH mb, neighbor
MATCH (mb)<-[:LOCATED_IN]-(c1:CrimeSuperSet), (neighbor)<-[:LOCATED_IN]-(c2:CrimeSuperSet)
RETURN mb.MB_CODE21, neighbor.MB_CODE21, c1.type, c2.type, count(c2)

MATCH (mb1:MeshBlock {MB_CODE21: '30119020000'})-[:NEAR*1..5]->(mb2:MeshBlock)
RETURN count(DISTINCT mb2)


MATCH (mb:MeshBlock {MB_CODE21: '30119020000'})-[:NEAR*1..3]->(neighbor)
WITH mb, neighbor
MATCH (mb)<-[:LOCATED_IN]-(c1:CrimeSuperSet), (neighbor)<-[:LOCATED_IN]-(c2:CrimeSuperSet)
RETURN mb.MB_CODE21, neighbor.MB_CODE21, c1.type, c2.type, count(c2)


MATCH (mb:MeshBlock {MB_CODE21: '30069530000'})
WITH mb
MATCH path = (mb)-[:INTERSECTS*1..106]->(mb2:MeshBlock)
WHERE ALL(i IN range(0, length(path) - 1) WHERE nodes(path)[i] <> nodes(path)[i+1])
WITH mb2, length(path) AS hop
MATCH (mb2)<-[:LOCATED_IN]-(css:CrimeSuperSet) 
RETURN hop, avg(css.count) AS average_crime_count
ORDER BY hop

SELECT 
    s.SA1_NAME_2021_x AS name, 
    COUNT(o.index) AS numberOfOffences
FROM 
    SA1 s
JOIN 
    meshblock m ON  s.sa1_code_2021 = m.sa1_code21
JOIN 
    offence o ON m.MB_CODE21 = o.MB_CODE21
GROUP BY 
    name
ORDER BY 
    numberOfOffences DESC;

 

# Find clusters of meshblocks with similar crime rates
MATCH (mb1:MeshBlock)-[:NEAR]->(mb2:MeshBlock)
WITH mb1, mb2
MATCH (mb1)<-[:LOCATED_IN]-(c1:CrimeSuperSet), (mb2)<-[:LOCATED_IN]-(c2:CrimeSuperSet)
WITH mb1, mb2, abs(sum(c1.count) - sum(c2.count)) AS crime_count_difference
WHERE crime_count_difference < 5
RETURN mb1.MB_CODE21, mb2.MB_CODE21, crime_count_difference

# Identify chains of connected meshblocks with increasing crime rates
MATCH path = (mb1:MeshBlock)-[:NEAR*]->(mb2:MeshBlock)
WITH path, [node in nodes(path) | sum((node)<-[:LOCATED_IN]-(c:CrimeSuperSet).count)] AS crime_counts
WHERE ALL(i IN range(0, length(crime_counts) - 2) WHERE crime_counts[i] < crime_counts[i+1])
RETURN path

# Analyze the impact of neighboring meshblocks on crime trends
MATCH (mb:MeshBlock)
WITH mb, collect(mb)-[:NEAR]->(neighbor) AS neighbors
UNWIND neighbors AS neighbor
MATCH (neighbor)<-[:LOCATED_IN]-(c:CrimeSuperSet)
WITH mb, avg(c.count) AS average_neighbor_crime_count
MATCH (mb)<-[:LOCATED_IN]-(c2:CrimeSuperSet)
RETURN mb.MB_CODE21, sum(c2.count) AS mb_crime_count, average_neighbor_crime_count


-- Step 1: Find neighbors within 1 hop
WITH one_hop_neighbors AS (
  SELECT
    mn.mb_code21_to
  FROM meshblock_neighbors AS mn
  WHERE
    mn.mb_code21_from = '30069530000'
),
-- Step 2: Find neighbors within 2 hops
two_hop_neighbors AS (
  SELECT
    mn.mb_code21_to
  FROM meshblock_neighbors AS mn
  WHERE
    mn.mb_code21_from IN (SELECT mb_code21_to FROM one_hop_neighbors)
)
-- Step 3: Combine neighbors from both hops
SELECT
  mb1.MB_CODE21,
  mb2.MB_CODE21,
  oc1.type,
  oc2.type,
  ST_Distance(mb1.centroid::geography, mb2.centroid::geography) AS distance
FROM meshblock AS mb1
JOIN meshblock AS mb2
  ON mb2.MB_CODE21 IN (SELECT mb_code21_to FROM one_hop_neighbors UNION SELECT mb_code21_to FROM two_hop_neighbors)
JOIN offences_agg AS oc1
  ON mb1.MB_CODE21 = oc1.MB_CODE21
JOIN offences_agg AS oc2
  ON mb2.MB_CODE21 = oc2.MB_CODE21
WHERE
  mb1.MB_CODE21 = '30069530000';



To COMPARE

WITH RECURSIVE meshblock_hops AS (
    SELECT mb_code21_from, mb_code21_to, 1 AS hop
    FROM meshblock_intersect
    WHERE mb_code21_from = '30562566300'
    UNION ALL
    SELECT mn.mb_code21_from, mn.mb_code21_to, mh.hop + 1
    FROM meshblock_intersect mn
    JOIN meshblock_hops mh ON mn.mb_code21_from = mh.mb_code21_to
    WHERE mh.hop < 10
)
-- Count the number of distinct neighboring meshblocks for each hop
SELECT hop, COUNT(DISTINCT mb_code21_to) AS neighbor_count
FROM meshblock_hops
GROUP BY hop
ORDER BY hop;

vs

MATCH (mb1:MeshBlock {MB_CODE21: '30562566300'})-[:INTERSECTS*1..10]->(mb2:MeshBlock)
RETURN count(DISTINCT mb2)




and another one following the highest crime path, that

then back to the list of sa1 queries, see if there is a square


MATCH path = (t1:Transport {name: 'Altandi'})-[:INTERSECTS*1..5]->(t2:Transport {name: 'Roma Street'})
RETURN path


MATCH path = (t1:Transport {name: 'Woolloongabba'})-[:LOCATED_IN]->(mb1:MeshBlock)-[:INTERSECTS*]->(mb2:MeshBlock)->(t2:Transport {name: 'Roma Street'})
RETURN t1, mb1, mb2
ASPLEY POLICE BEAT SHOPFRONT 30562940100

---

MATCH (start:Transport {name: 'Domestic Terminal'})-[:LOCATED_IN]->(mb:MeshBlock)
WITH mb
MATCH path = (mb)-[:INTERSECTS*1..4]->(mb2:MeshBlock)
WITH mb2, length(path) AS hop
MATCH (mb2)<-[:LOCATED_IN]-(css:CrimeSuperSet) 
RETURN hop, avg(css.count) AS average_crime_count
ORDER BY hop

vs


-- Recursive CTE to traverse meshblock neighbors up to 4 hops
WITH RECURSIVE meshblock_hops AS (
    SELECT mb_code21_from, mb_code21_to, 1 AS hop
    FROM meshblock_intersect
    WHERE mb_code21_from = (SELECT MB_CODE21 FROM transport WHERE name = 'Domestic Terminal') -- Starting meshblock
    UNION ALL
    SELECT mn.mb_code21_from, mn.mb_code21_to, mh.hop + 1
    FROM meshblock_intersect mn
    JOIN meshblock_hops mh ON mn.mb_code21_from = mh.mb_code21_to
    WHERE mh.hop < 4  -- Limit to 4 hops
)
-- Select relevant information and aggregate crime counts for each hop, limiting the results to the top 5
SELECT 
    hop, 
    AVG(COALESCE(oc.count, 0)) AS average_crime_count
FROM meshblock_hops mh
LEFT JOIN offences_agg oc ON mh.mb_code21_to = oc.MB_CODE21
GROUP BY hop
ORDER BY hop



-----

-- Recursive CTE to traverse meshblock neighbors up to 5 hops
WITH RECURSIVE meshblock_hops AS (
    SELECT mb_code21_from, mb_code21_to, 1 AS hop
    FROM meshblock_intersect
    WHERE mb_code21_from = (SELECT MB_CODE21 FROM transport WHERE name = 'Domestic Terminal') -- Starting meshblock
    UNION ALL
    SELECT mn.mb_code21_from, mn.mb_code21_to, mh.hop + 1
    FROM meshblock_intersect mn
    JOIN meshblock_hops mh ON mn.mb_code21_from = mh.mb_code21_to
    WHERE mh.hop < 5  -- Limit to 5 hops
),
-- Calculate total crime count for each meshblock within 5 hops
meshblock_crime_counts AS (
  SELECT
    mh.mb_code21_to,
    SUM(oa.count) AS offence_count,
    mh.hop  -- Include hop in the meshblock_crime_counts CTE
  FROM meshblock_hops mh
  LEFT JOIN offences_agg oa ON mh.mb_code21_to = oa.MB_CODE21
  GROUP BY mh.mb_code21_to, mh.hop
),
-- Rank meshblocks by crime count within each hop
ranked_meshblocks AS (
  SELECT
    mb_code21_to,
    offence_count,
    hop,
    ROW_NUMBER() OVER (PARTITION BY hop ORDER BY offence_count DESC) as rn
  FROM meshblock_crime_counts
)
-- Select the meshblock with the highest crime count at each hop
SELECT mb_code21_to
FROM ranked_meshblocks
WHERE rn = 1
ORDER BY hop ASC;


those two and a couple sa1s then done!




MATCH (mb1:MeshBlock)-[:INTERSECTS]->(mb2:MeshBlock)
MATCH (mb1)<-[:LOCATED_IN]-(c1:CrimeSuperSet), (mb2)<-[:LOCATED_IN]-(c2:CrimeSuperSet)
WITH mb1, mb2, collect(c1.count) AS c1_counts, collect(c2.count) AS c2_counts
WHERE reduce(acc1 = 0, x IN c1_counts | acc1 + x) < reduce(acc2 = 0, y IN c2_counts | acc2 + y)
RETURN mb1.MB_CODE21 AS start_mb_code21, 
       mb2.MB_CODE21 AS next_mb_code21, 
       [mb1.MB_CODE21, mb2.MB_CODE21] AS path, 
       reduce(acc1 = 0, x IN c1_counts | acc1 + x) AS start_crime_count, 
       reduce(acc2 = 0, y IN c2_counts | acc2 + y) AS next_crime_count



MATCH (mb1:MeshBlock {MB_CODE21: '30562566300'})-[:INTERSECTS]->(mb2:MeshBlock)
MATCH (c1:CrimeSuperSet)-[:LOCATED_IN]->(mb1)
match (c2:CrimeSuperSet)-[:LOCATED_IN]->(mb2)
WITH mb1, mb2, collect(c1.count) AS c1_counts, collect(c2.count) AS c2_counts
WHERE reduce(acc1 = 0, x IN c1_counts | acc1 + x) < reduce(acc2 = 0, y IN c2_counts | acc2 + y)
RETURN mb1.MB_CODE21 AS start_mb_code21, 
       mb2.MB_CODE21 AS next_mb_code21, 
       [mb1.MB_CODE21, mb2.MB_CODE21] AS path, 
       reduce(acc1 = 0, x IN c1_counts | acc1 + x) AS start_crime_count, 
       reduce(acc2 = 0, y IN c2_counts | acc2 + y) AS next_crime_count


-- Recursive CTE to traverse meshblock neighbors
WITH RECURSIVE meshblock_hops AS (
    -- Base case: Start with pairs of neighboring meshblocks
    SELECT 
        mn.mb_code21_from AS start_mb_code21,
        mn.mb_code21_to AS next_mb_code21,
        ARRAY[mn.mb_code21_from, mn.mb_code21_to] AS path,
        oc1.count AS start_crime_count,
        oc2.count AS next_crime_count
    FROM 
        meshblock_neighbors mn
    JOIN 
        offences_agg oc1 ON mn.mb_code21_from = oc1.MB_CODE21
    JOIN 
        offences_agg oc2 ON mn.mb_code21_to = oc2.MB_CODE21
    WHERE 
        oc1.count < oc2.count  -- Start with meshblocks with increasing crime counts

    UNION ALL

    -- Recursive case: Extend the chain with new meshblocks
    SELECT 
        ch.start_mb_code21,
        mn.mb_code21_to,
        ch.path || mn.mb_code21_to,
        ch.start_crime_count,
        oc2.count
    FROM 
        meshblock_hops ch
    JOIN 
        meshblock_neighbors mn ON ch.next_mb_code21 = mn.mb_code21_from
    JOIN 
        offences_agg oc2 ON mn.mb_code21_to = oc2.MB_CODE21
    WHERE 
        ch.next_crime_count < oc2.count  -- Ensure crime count keeps increasing
        AND NOT mn.mb_code21_to = ANY(ch.path)  -- Prevent cycles
)
-- Final output: Return the chains
SELECT 
    path AS chain_of_meshblocks,
    ARRAY_LENGTH(path, 1) AS chain_length
FROM 
    meshblock_hops
ORDER BY 
    chain_length DESC;\


-- Recursive CTE to traverse meshblock neighbors
WITH RECURSIVE meshblock_hops AS (
-- Base case: Start with pairs of neighboring meshblocks
SELECT
mn.mb_code21_from AS start_mb_code21,
mn.mb_code21_to AS next_mb_code21,
ARRAY[mn.mb_code21_from, mn.mb_code21_to] AS path,
oc1.count AS start_crime_count,
oc2.count AS next_crime_count
FROM
meshblock_neighbors mn
JOIN
offences_agg oc1 ON mn.mb_code21_from = oc1.MB_CODE21
JOIN
offences_agg oc2 ON mn.mb_code21_to = oc2.MB_CODE21
WHERE
oc1.count < oc2.count -- Start with meshblocks with increasing crime counts

UNION ALL

-- Recursive case: Extend the chain with new meshblocks
SELECT
ch.start_mb_code21,
mn.mb_code21_to,
ch.path || mn.mb_code21_to,
ch.start_crime_count,
oc2.count
FROM
meshblock_hops ch
JOIN
meshblock_neighbors mn ON ch.next_mb_code21 = mn.mb_code21_from
JOIN
offences_agg oc2 ON mn.mb_code21_to = oc2.MB_CODE21
WHERE
ch.next_crime_count < oc2.count -- Ensure crime count keeps increasing
AND NOT mn.mb_code21_to = ANY(ch.path) -- Prevent cycles
)
-- Final output: Return the chains
SELECT
path AS chain_of_meshblocks,
ARRAY_LENGTH(path, 1) AS chain_length
FROM
meshblock_hops
ORDER BY
chain_length DESC;



MATCH (mb1:MeshBlock {MB_CODE21: '30119020000'})-[:INTERSECTS*1..5]->(mb2:MeshBlock)
MATCH (mb1)<-[:LOCATED_IN]-(c1:CrimeSuperSet), (mb2)<-[:LOCATED_IN]-(c2:CrimeSuperSet)
WITH mb1, mb2, collect(c1.count) AS c1_counts, collect(c2.count) AS c2_counts
WHERE reduce(acc1 = 0, x IN c1_counts | acc1 + x) < reduce(acc2 = 0, y IN c2_counts | acc2 + y)
RETURN mb1.MB_CODE21 AS start_mb_code21, 
       mb2.MB_CODE21 AS next_mb_code21, 
       [mb1.MB_CODE21, mb2.MB_CODE21] AS path, 
       reduce(acc1 = 0, x IN c1_counts | acc1 + x) AS start_crime_count, 
       reduce(acc2 = 0, y IN c2_counts | acc2 + y) AS next_crime_count



MATCH (mb1:MeshBlock {MB_CODE21: '30119020000'})-[:INTERSECTS]->(mb2:MeshBlock)
MATCH (mb1)<-[:LOCATED_IN]-(c1:CrimeSuperSet)
match (mb2)<-[:LOCATED_IN]-(c2:CrimeSuperSet)
WITH mb1, mb2, sum(c1.count) AS start_crime_count, sum(c2.count) AS next_crime_count
WHERE start_crime_count < next_crime_count
RETURN mb1.MB_CODE21 AS start_mb_code21, 
       mb2.MB_CODE21 AS next_mb_code21, 
       [mb1.MB_CODE21, mb2.MB_CODE21] AS path, 
       start_crime_count, 
       next_crime_count




---

MATCH path = (mb1:MeshBlock)-[:INTERSECTS*1..2]->(mb2:MeshBlock)
WHERE mb1.MB_CODE21 = '30119020000'
  AND ALL(i IN range(0, length(path) - 2) 
          WHERE nodes(path)[i].crime_count < nodes(path)[i+1].crime_count
     )
RETURN path


---



MATCH (mb1:MeshBlock {MB_CODE21: '30119020000'})-[:INTERSECTS*1..1]->(mb2:MeshBlock)
WITH mb1, mb2
MATCH (mb1)<-[:LOCATED_IN]-(c1:CrimeSuperSet)
match (mb2)<-[:LOCATED_IN]-(c2:CrimeSuperSet)
WITH DISTINCT mb1, mb2, c1, c2  // Add DISTINCT here to avoid duplicate rows
WITH mb1, mb2, sum(c1.count) AS start_crime_count, sum(c2.count) AS next_crime_count
WHERE start_crime_count < next_crime_count
RETURN mb1.MB_CODE21 AS start_mb_code21, 
       mb2.MB_CODE21 AS next_mb_code21, 
       [mb1.MB_CODE21, mb2.MB_CODE21] AS path, 
       start_crime_count, 
       next_crime_count

       vs

WITH RECURSIVE reachable_meshblocks AS (
    SELECT 
        mb1.MB_CODE21 AS mb1_code, 
        mb2.MB_CODE21 AS mb2_code,
        ARRAY[mb1.MB_CODE21] AS path,
        1 AS level  -- Keep track of the path length
    FROM meshblock mb1
    JOIN meshblock_intersect mi ON mb1.MB_CODE21 = mi.mb_code21_from
    JOIN meshblock mb2 ON mi.mb_code21_to = mb2.MB_CODE21
    WHERE mb1.MB_CODE21 = '30119020000'

    UNION ALL

    SELECT 
        rm.mb1_code, 
        mb3.MB_CODE21,
        rm.path || mb3.MB_CODE21,
        rm.level + 1
    FROM reachable_meshblocks rm
    JOIN meshblock_intersect mi ON rm.mb2_code = mi.mb_code21_from
    JOIN meshblock mb3 ON mi.mb_code21_to = mb3.MB_CODE21
    WHERE NOT rm.mb2_code = ANY(rm.path)  -- Prevent cycles
      AND rm.level < 100
), 
crime_counts AS (
  SELECT
        rm.mb1_code,
        rm.mb2_code,
        rm.path,
        COALESCE(SUM(c1.count), 0) AS start_crime_count,
        COALESCE(SUM(c2.count), 0) AS next_crime_count
    FROM reachable_meshblocks rm
    LEFT JOIN offences_agg c1 ON rm.mb1_code = c1.MB_CODE21
    LEFT JOIN offences_agg c2 ON rm.mb2_code = c2.MB_CODE21
    GROUP BY rm.mb1_code, rm.mb2_code, rm.path
)
SELECT 
    mb1_code AS start_mb_code21,
    mb2_code AS next_mb_code21,
    path,
    start_crime_count,
    next_crime_count
FROM crime_counts
WHERE start_crime_count < next_crime_count;


NAH




-- Recursive CTE to find paths of meshblocks within 2 hops
WITH RECURSIVE meshblock_paths AS (
    -- Base case: Start with the initial meshblock and its immediate neighbors
    SELECT 
        mn.mb_code21_from,
        mn.mb_code21_to,
        ARRAY[mn.mb_code21_from, mn.mb_code21_to] AS path,
        1 AS hop
    FROM 
        meshblock_intersect mn
    WHERE 
        mn.mb_code21_from = '30119020000'

    UNION ALL

    -- Recursive case: Extend the path with neighboring meshblocks
    SELECT 
        mp.mb_code21_from,
        mn.mb_code21_to,
        mp.path || mn.mb_code21_to,
        mp.hop + 1
    FROM 
        meshblock_paths mp -- This should reference the CTE itself, not a table
    JOIN 
        meshblock_intersect mn ON mp.mb_code21_to = mn.mb_code21_from
    WHERE 
        mp.hop < 1  -- Limit to 2 hops
        AND NOT mn.mb_code21_to = ANY(mp.path)  -- Prevent cycles
),
-- Calculate crime counts for the starting and ending meshblocks in each path
meshblock_crime_counts AS (
    SELECT 
        mp.mb_code21_from AS start_mb_code21,  -- Use mb_code21_from for the starting meshblock
        mp.mb_code21_to AS next_mb_code21,  -- Use mb_code21_to for the last meshblock
        mp.path,
        (SELECT SUM(count) FROM offences_agg WHERE MB_CODE21 = mp.mb_code21_from) AS start_crime_count,
        (SELECT SUM(count) FROM offences_agg WHERE MB_CODE21 = mp.mb_code21_to) AS next_crime_count
    FROM 
        meshblock_paths mp
)
-- Filter for paths where the crime count increases
SELECT 
    start_mb_code21,
    next_mb_code21,
    path,
    start_crime_count,
    next_crime_count
FROM 
    meshblock_crime_counts
WHERE 
    start_crime_count < next_crime_count;

---


MATCH path = (mb1:MeshBlock)-[:INTERSECTS*1]->(mb2:MeshBlock)
WHERE mb1.MB_CODE21 = '30119020000'
  AND ALL(i IN range(0, length(path) - 2) 
          WHERE nodes(path)[i].crime_count < nodes(path)[i+1].crime_count
     )
UNWIND nodes(path) AS node
MATCH (node)<-[:LOCATED_IN]-(c1:CrimeSuperSet)
RETURN node.MB_CODE21, sum(c1.count) AS crime_count

vs



-- Recursive CTE to find paths of meshblocks within 5 hops
WITH RECURSIVE meshblock_paths AS (
    -- Base case: Start with the initial meshblock and its immediate neighbors
    SELECT 
        mn.mb_code21_from,
        mn.mb_code21_to,
        ARRAY[mn.mb_code21_from, mn.mb_code21_to] AS path,
        1 AS hop
    FROM 
        meshblock_intersect mn
    WHERE 
        mn.mb_code21_from = '30119020000'

    UNION ALL

    -- Recursive case: Extend the path with neighboring meshblocks
    SELECT 
        mp.mb_code21_from,
        mn.mb_code21_to,
        mp.path || mn.mb_code21_to,
        mp.hop + 1
    FROM 
        meshblock_paths mp
    JOIN 
        meshblock_intersect mn ON mp.mb_code21_to = mn.mb_code21_from
    WHERE 
        mp.hop < 5  -- Limit to 5 hops
        AND NOT mn.mb_code21_to = ANY(mp.path)  -- Prevent cycles
),
-- Extract individual meshblocks from the paths
meshblock_nodes AS (
    SELECT 
        unnest(path) AS MB_CODE21
    FROM 
        meshblock_paths
)
-- Join with offences_agg to get crime counts for each meshblock
SELECT 
    mn.MB_CODE21,
    COALESCE(SUM(oa.count), 0) AS crime_count
FROM 
    meshblock_nodes mn
LEFT JOIN 
    offences_agg oa ON mn.MB_CODE21 = oa.MB_CODE21
GROUP BY 
    mn.MB_CODE21
ORDER BY 
    mn.MB_CODE21;

    ``


-----

MATCH (p:Transport)-[:LOCATED_IN]->(mb:MeshBlock)-[:INTERSECTS*0..2]->(mb2:MeshBlock)
WITH p, mb2
MATCH (mb2)<-[:LOCATED_IN]-(c:CrimeSuperSet)
RETURN p.name, avg(c.count) AS average_crime_count


vs

  WITH RECURSIVE reachable_meshblocks AS (
  SELECT
    mb.MB_CODE21,
    p.name
  FROM meshblock AS mb
  JOIN transport AS p
    ON mb.MB_CODE21 = p.MB_CODE21
  
  UNION
  
  SELECT
    mb2.MB_CODE21,
    reachable_meshblocks.name
  FROM reachable_meshblocks
  JOIN meshblock_neighbors AS mn
    ON reachable_meshblocks.MB_CODE21 = mn.mb_code21_from
  JOIN meshblock AS mb2
    ON mn.mb_code21_to = mb2.MB_CODE21
)
SELECT
  reachable_meshblocks.name,
  AVG(cs.count) AS average_crime_count
FROM reachable_meshblocks
JOIN offences_agg AS cs
  ON reachable_meshblocks.MB_CODE21 = cs.MB_CODE21
GROUP BY
  reachable_meshblocks.name;