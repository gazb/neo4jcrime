import neo4j
from modules.neo4j.credentials import neo4j_credentials


driver = neo4j.GraphDatabase.driver(
    neo4j_credentials['url'], 
    auth=(neo4j_credentials['username'], neo4j_credentials['password'])
)

# driver = neo4j.GraphDatabase.driver(NEO4J_URI, auth=(NEO4J_USER, NEO4J_PASSWORD))
CYPHER_QUERY = """
MATCH (n) RETURN COUNT(n) AS node_count
"""

def get_node_count(tx):
    results = tx.run(CYPHER_QUERY)
    df = results.to_df()
    return df

with driver.session() as session:
    df = session.execute_read(get_node_count)
    print(df)
    driver.close()
