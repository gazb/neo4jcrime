1. 

Total crimes

```
SELECT cs.type,SUM(cs.count) AS TotalCrimes
FROM public.offences_agg cs
GROUP BY cs.type
ORDER BY TotalCrimes DESC;
```
```
MATCH (c:CrimeSuperSet)
RETURN c.type, SUM(c.count) AS TotalCrimes
ORDER BY TotalCrimes DESC
```