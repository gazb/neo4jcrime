todo use new endpoints
https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_meshblocks.csv
https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_offences_agg.csv
https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_offensives_from_2024-04-01_in_bris.csv
https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_parks.csv
https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_police.csv
https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_sa1.csv
https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_trains_busses.csv

1. Load meshblocks

```
CREATE CONSTRAINT UniqueMeshblock FOR (mb:MeshBlock) REQUIRE mb.MB_CODE21 IS UNIQUE
```

```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_meshblocks.csv' as mesh 
wITH *
MERGE(mb:MeshBlock {MB_CODE21: mesh.MB_CODE21})
SET mb += mesh

<!-- ON CREATE SET mb = mesh 
ON MATCH SET mb += mesh  -->
```

Create the points
```
MATCH (n:MeshBlock) 
set n.center = point({longitude: toFloat(n.longitude), latitude: toFloat(n.latitude)}) 

```


2. Load crime.
```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_offences_agg.csv' as offence 
MATCH (mb:MeshBlock { MB_CODE21: offence.MB_CODE21}) 
CREATE (cs:CrimeSuperSet { 
    count: toInteger(offence.count),
    date: offence.monthyear, 
    type: offence.Type,
    MB_CODE21: offence.meshblock
})
CREATE (cs)-[:LOCATED_IN]->(mb) 
```
<!-- ```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_offences_agg.tsv' as offence FIELDTERMINATOR '\t'
MATCH (mb:MeshBlock { MB_CODE21: offence.meshblock}) 
CREATE (cs:CrimeSet { count: offence.count, date: offence.monthyear, meshblock: offence.meshblock })
MERGE(ct:OffenceType {name: offence.Type})
MERGE (cs)-[:APPEARS_IN]->(mb)
MERGE (cs)-[hct:HAS_OFFENCE_TYPE]->(ct)
``` -->
```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_offensives_from_2024-04-01_in_bris.csv' as offence
MATCH (mb:MeshBlock { MB_CODE21: offence.MB_CODE21}) 
CREATE (of:Offence { 
    date: offence.Date, 
    type: offence.Type,
    MB_CODE21: offence.MB_CODE21
})
CREATE (of)-[:LOCATED_IN]->(mb) 
```

3. Load SA1s (twice, two different sa1 datasets with different info)

```
CREATE CONSTRAINT UniqueSA1 FOR (p:SA1) REQUIRE p.SA1_CODE_2021 IS UNIQUE
```

```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_gdfBris_sa1.csv' as saonegzerocsv 
wITH *
MERGE(saonegzero:SA1 {SA1_CODE_2021: saonegzerocsv.SA1_CODE_2021})
SET saonegzero += saonegzerocsv
```
```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_gdfBris_sa1_go2.csv' as sa1g1s
wITH *
MERGE(saonegone:SA1 {SA1_CODE_2021: sa1g1s.SA1_CODE_2021})
SET saonegone += sa1g1s
```
And create the point
```
MATCH (n:SA1) 
set n.center = point({longitude: toFloat(n.longitude), latitude: toFloat(n.latitude)})
```
Create the relationship
```
MATCH (n:SA1)
MATCH(mb:MeshBlock { SA1_CODE21: n.SA1_CODE_2021})
MERGE(mb)-[i:BELONGS_TO_SA1]->(n)
```

4. Police stations and police beats
```
CREATE CONSTRAINT UniquePoliceStation FOR (p:PoliceStation) REQUIRE p.NAME IS UNIQUE
```

```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_police' AS row
MERGE (police:PoliceStation {
    NAME: row.NAME,
    location: point({latitude: toFloat(row.latitude), longitude: toFloat(row.longitude)}),
})
SET police += row
```

```
MATCH (n:PoliceStation) 
set n.center = point({longitude: toFloat(n.longitude), latitude: toFloat(n.latitude)})
```
```
<!-- CREATE SPATIAL INDEX police_station_spatial_index FOR (p:PoliceStation) ON p.location -->
CREATE POINT INDEX FOR (n:PoliceStation) ON (n.center)
```

```
MATCH (n:PoliceStation)
MATCH(mb:MeshBlock { MB_CODE21: n.MB_CODE21})
MERGE(n)-[i:LOCATED_IN]->(mb)
```

Test it out
```
MATCH(n:PoliceStation)-[i:LOCATED_IN]->(mb:MeshBlock)-[b:BELONGS_TO_SA1]->(sa:SA1)
return n, i, mb, b ,sa
```

5. Transport

```
CREATE CONSTRAINT UniqueTransport FOR (mb:Transport) REQUIRE mb.name IS UNIQUE
```
```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_trains_busses.csv' AS row
MERGE (a:Transport {name: row.name})
SET a += row
```

```
MATCH (n:Transport)
MATCH(mb:MeshBlock { MB_CODE21: n.MB_CODE21})
MERGE(n)-[i:LOCATED_IN]->(mb)
```

```
MATCH (n:Transport) 
set n.center = point({longitude: toFloat(n.longitude), latitude: toFloat(n.latitude)})
```


6. Parks


```
CREATE CONSTRAINT UniquePark FOR (mb:Park) REQUIRE mb.PARK_NAME IS UNIQUE
```

```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_parks.csv' AS row
MERGE (a:Park {PARK_NAME: row.PARK_NAME})
SET a += row
```
```
MATCH (n:Park)
MATCH(mb:MeshBlock { MB_CODE21: n.MB_CODE21})
MERGE(n)-[i:LOCATED_IN]->(mb)
```
```
MATCH (n:Park) 
set n.center = point({longitude: toFloat(n.longitude), latitude: toFloat(n.latitude)})
```


7. indexing
```
CREATE POINT INDEX FOR (n:MeshBlock) ON (n.center)
CREATE POINT INDEX FOR (n:Transport) ON (n.center)
CREATE POINT INDEX FOR (n:PoliceStation) ON (n.center)
CREATE POINT INDEX FOR (n:Park) ON (n.center)
```

Pre calculated near relationship
```
MATCH (mb1:MeshBlock), (mb2:MeshBlock)
WHERE mb1 <> mb2 AND point.distance(mb1.center, mb2.center) < 100
MERGE (mb1)-[:NEAR {type: 'meshblock', distance: point.distance(mb1.center, mb2.center), weight: 1 / point.distance(mb1.center, mb2.center)}]->(mb2)


// Add NEAR relationships between MeshBlocks and Parks within 100m, with weight based on inverse distance
MATCH (mb:MeshBlock), (p:Park)
WHERE point.distance(mb.center, p.center) < 100
MERGE (mb)-[:NEAR {type: 'park', distance: point.distance(mb.center, p.center), weight: 1 / point.distance(mb.center, p.center)}]->(p)

// Add NEAR relationships between MeshBlocks and Transport (Train Stations) within 2km, with weight based on inverse distance
MATCH (mb:MeshBlock), (t:Transport {feature_ty: 'Railway Station'})
WHERE point.distance(mb.center, t.center) < 100
MERGE (mb)-[:NEAR {type: 'train_station', distance: point.distance(mb.center, t.center), weight: 1 / point.distance(mb.center, t.center)}]->(t)

// Add NEAR relationships between MeshBlocks and Police Stations within 10km, with weight based on inverse distance
MATCH (mb:MeshBlock), (ps:PoliceStation)
WHERE point.distance(mb.center, ps.center) < 100
MERGE (mb)-[:NEAR {type: 'police_station', distance: point.distance(mb.center, ps.center), weight: 1 / point.distance(mb.center, ps.center)}]->(ps)
```

## Queries

MATCH (n:PoliceStation)-[i:LOCATED_IN]->(mb:MeshBlock)<-[b:LOCATED_IN]-(cs:CrimeSuperSet)
return n, i, mb, b ,cs
limit 20


Find the closest police station to a park
```
MATCH (p:Park ), (ps:PoliceStation)
WITH p, ps, point.distance(p.center, ps.center) AS dist
ORDER BY dist ASC
LIMIT 5
RETURN p.PARK_NAME, ps.NAME, dist
```





To test you can get the distance between two
```
MATCH (n:MeshBlock) with n order by rand() limit 1
MATCH(b:MeshBlock) with n, b order by rand() limit 1
return n.SA2_NAME21, b.SA2_NAME21, 
       point.distance(point({longitude: n.center.x, latitude: n.center.y}), point({longitude: b.center.x, latitude: b.center.y})) / 1000 as km 
```

### note the difference between these two, should use lat longs for points

```
MATCH (mb1:MeshBlock {MB_CODE21: '30025980000'}), (mb2:MeshBlock {MB_CODE21: '30025970000'})
RETURN mb1.SA2_NAME21, mb2.SA2_NAME21, point.distance(point({x: mb1.center.x, latitude: mb1.center.y}), point({y: mb2.center.x, latitude: mb2.center.y}))
```
```
MATCH (mb1:MeshBlock {MB_CODE21: '30025980000'}), (mb2:MeshBlock {MB_CODE21: '30025970000'})
RETURN mb1.SA2_NAME21, mb2.SA2_NAME21, point.distance(mb1.center, mb2.center)
```
e.g. in sql this would be
```
WITH random_meshblock AS (
  SELECT
    SA2_NAME21,
    centroid
  FROM meshblock
  ORDER BY RANDOM()
  LIMIT 1
), random_meshblock_pair AS (
  SELECT
    rmb.SA2_NAME21 AS SA2_NAME21_first,
    mb.SA2_NAME21 AS SA2_NAME21_second,
    rmb.centroid AS centroid_first,
    mb.centroid AS centroid_second
  FROM random_meshblock AS rmb
  JOIN meshblock AS mb
    ON rmb.SA2_NAME21 <> mb.SA2_NAME21
  ORDER BY RANDOM()
  LIMIT 1
)
SELECT
  SA2_NAME21_first,
  SA2_NAME21_second,
  ST_Distance(centroid_first::geography, centroid_second::geography) / 1000 AS km
FROM random_meshblock_pair;
```

Checking distance values match
```
  SELECT
    p.PARK_NAME,
	mb.mb_code21,
    ST_Distance(mb.centroid::geography, p.centroid::geography) as dist
  FROM parks AS p, meshblock AS mb
	where p.park_name = 'STANLEY PARK' and mb.mb_code21 = '30025980000'
	```
    ```
MATCH (p:Park {PARK_NAME: 'STANLEY PARK'}), (mb:MeshBlock {MB_CODE21: '30025980000'})
RETURN p.PARK_NAME, mb.MB_CODE21, point.distance(p.center, mb.center) AS dist
```



```
SELECT
  mb1.SA2_NAME21,
  mb2.SA2_NAME21,
  ST_Distance(mb1.centroid::geography, mb2.centroid::geography) / 1000 AS km
FROM meshblock AS mb1
JOIN meshblock AS mb2
  ON mb1.mb_code21 = '30025980000' AND mb2.mb_code21 = '30026202000';
  ```


  ```
  MATCH (mb1:MeshBlock {MB_CODE21: '30025980000'}), (mb2:MeshBlock {MB_CODE21: '30026202000'})
RETURN mb1.SA2_NAME21, mb2.SA2_NAME21, point.distance(mb1.center, mb2.center) / 1000 AS km
```