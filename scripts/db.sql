-- Create the database
-- CREATE DATABASE crime_data;

-- -- Connect to the database
-- \c crime_data;

-- Create the tables
-- Create the combined transport_stations table with all attributes
CREATE TABLE meshblock (
    MB_CODE21 TEXT PRIMARY KEY,
    MB_CAT21 TEXT,
    CHG_FLAG21 INTEGER,
    CHG_LBL21 TEXT,
    SA1_CODE21 TEXT REFERENCES sa1(SA1_CODE_2021),
    SA2_CODE21 TEXT,
    SA2_NAME21 TEXT,
    SA3_CODE21 TEXT,
    SA3_NAME21 TEXT,
    SA4_CODE21 TEXT,
    SA4_NAME21 TEXT,
    GCC_CODE21 TEXT,
    GCC_NAME21 TEXT,
    STE_CODE21 TEXT,
    STE_NAME21 TEXT,
    AUS_CODE21 TEXT,
    AUS_NAME21 TEXT,
    AREASQKM21 NUMERIC,
    LOCI_URI21 TEXT,
    geometry GEOMETRY,
    longitude NUMERIC,
    latitude NUMERIC
);

-- Create the combined gdfBris_sa1 table with all attributes
CREATE TABLE sa1 (
    SA1_CODE_2021 TEXT PRIMARY KEY,
    SA1_NAME_2021 TEXT,
    Median_age_persons NUMERIC,
    Median_mortgage_repay_monthly NUMERIC,
    Median_tot_prsnl_inc_weekly NUMERIC,
    Median_rent_weekly NUMERIC,
    Median_tot_fam_inc_weekly NUMERIC,
    Average_num_psns_per_bedroom NUMERIC,
    Median_tot_hhd_inc_weekly NUMERIC,
    Average_household_size NUMERIC,
    Tot_P_M INTEGER,
    Tot_P_F INTEGER,
    Tot_P_P INTEGER,
    Age_0_4_yr_M INTEGER,
    Age_0_4_yr_F INTEGER,
    Age_0_4_yr_P INTEGER,
    Age_5_14_yr_M INTEGER,
    Age_5_14_yr_F INTEGER,
    Age_5_14_yr_P INTEGER,
    Age_15_19_yr_M INTEGER,
    Age_15_19_yr_F INTEGER,
    Age_15_19_yr_P INTEGER,
    Age_20_24_yr_M INTEGER,
    Age_20_24_yr_F INTEGER,
    Age_20_24_yr_P INTEGER,
    Age_25_34_yr_M INTEGER,
    Age_25_34_yr_F INTEGER,
    Age_25_34_yr_P INTEGER,
    Age_35_44_yr_M INTEGER,
    Age_35_44_yr_F INTEGER,
    Age_35_44_yr_P INTEGER,
    Age_45_54_yr_M INTEGER,
    Age_45_54_yr_F INTEGER,
    Age_45_54_yr_P INTEGER,
    Age_55_64_yr_M INTEGER,
    Age_55_64_yr_F INTEGER,
    Age_55_64_yr_P INTEGER,
    Age_65_74_yr_M INTEGER,
    Age_65_74_yr_F INTEGER,
    Age_65_74_yr_P INTEGER,
    Age_75_84_yr_M INTEGER,
    Age_75_84_yr_F INTEGER,
    Age_75_84_yr_P INTEGER,
    Age_85ov_M INTEGER,
    Age_85ov_F INTEGER,
    Age_85ov_P INTEGER,
    Counted_Census_Night_home_M INTEGER,
    Counted_Census_Night_home_F INTEGER,
    Counted_Census_Night_home_P INTEGER,
    Count_Census_Nt_Ewhere_Aust_M INTEGER,
    Count_Census_Nt_Ewhere_Aust_F INTEGER,
    Count_Census_Nt_Ewhere_Aust_P INTEGER,
    Indigenous_psns_Aboriginal_M INTEGER,
    Indigenous_psns_Aboriginal_F INTEGER,
    Indigenous_psns_Aboriginal_P INTEGER,
    Indig_psns_Torres_Strait_Is_M INTEGER,
    Indig_psns_Torres_Strait_Is_F INTEGER,
    Indig_psns_Torres_Strait_Is_P INTEGER,
    Indig_Bth_Abor_Torres_St_Is_M INTEGER,
    Indig_Bth_Abor_Torres_St_Is_F INTEGER,
    Indig_Bth_Abor_Torres_St_Is_P INTEGER,
    Indigenous_P_Tot_M INTEGER,
    Indigenous_P_Tot_F INTEGER,
    Indigenous_P_Tot_P INTEGER,
    Birthplace_Australia_M INTEGER,
    Birthplace_Australia_F INTEGER,
    Birthplace_Australia_P INTEGER,
    Birthplace_Elsewhere_M INTEGER,
    Birthplace_Elsewhere_F INTEGER,
    Birthplace_Elsewhere_P INTEGER,
    Lang_used_home_Eng_only_M INTEGER,
    Lang_used_home_Eng_only_F INTEGER,
    Lang_used_home_Eng_only_P INTEGER,
    Lang_used_home_Oth_Lang_M INTEGER,
    Lang_used_home_Oth_Lang_F INTEGER,
    Lang_used_home_Oth_Lang_P INTEGER,
    Australian_citizen_M INTEGER,
    Australian_citizen_F INTEGER,
    Australian_citizen_P INTEGER,
    Age_psns_att_educ_inst_0_4_M INTEGER,
    Age_psns_att_educ_inst_0_4_F INTEGER,
    Age_psns_att_educ_inst_0_4_P INTEGER,
    Age_psns_att_educ_inst_5_14_M INTEGER,
    Age_psns_att_educ_inst_5_14_F INTEGER,
    Age_psns_att_educ_inst_5_14_P INTEGER,
    Age_psns_att_edu_inst_15_19_M INTEGER,
    Age_psns_att_edu_inst_15_19_F INTEGER,
    Age_psns_att_edu_inst_15_19_P INTEGER,
    Age_psns_att_edu_inst_20_24_M INTEGER,
    Age_psns_att_edu_inst_20_24_F INTEGER,
    Age_psns_att_edu_inst_20_24_P INTEGER,
    Age_psns_att_edu_inst_25_ov_M INTEGER,
    Age_psns_att_edu_inst_25_ov_F INTEGER,
    Age_psns_att_edu_inst_25_ov_P INTEGER,
    High_yr_schl_comp_Yr_12_eq_M INTEGER,
    High_yr_schl_comp_Yr_12_eq_F INTEGER,
    High_yr_schl_comp_Yr_12_eq_P INTEGER,
    High_yr_schl_comp_Yr_11_eq_M INTEGER,
    High_yr_schl_comp_Yr_11_eq_F INTEGER,
    High_yr_schl_comp_Yr_11_eq_P INTEGER,
    High_yr_schl_comp_Yr_10_eq_M INTEGER,
    High_yr_schl_comp_Yr_10_eq_F INTEGER,
    High_yr_schl_comp_Yr_10_eq_P INTEGER,
    High_yr_schl_comp_Yr_9_eq_M INTEGER,
    High_yr_schl_comp_Yr_9_eq_F INTEGER,
    High_yr_schl_comp_Yr_9_eq_P INTEGER,
    High_yr_schl_comp_Yr_8_belw_M INTEGER,
    High_yr_schl_comp_Yr_8_belw_F INTEGER,
    High_yr_schl_comp_Yr_8_belw_P INTEGER,
    High_yr_schl_comp_D_n_g_sch_M INTEGER,
    High_yr_schl_comp_D_n_g_sch_F INTEGER,
    High_yr_schl_comp_D_n_g_sch_P INTEGER,
    Count_psns_occ_priv_dwgs_M INTEGER,
    Count_psns_occ_priv_dwgs_F INTEGER,
    Count_psns_occ_priv_dwgs_P INTEGER,
    Count_Persons_other_dwgs_M INTEGER,
    Count_Persons_other_dwgs_F INTEGER,
    Count_Persons_other_dwgs_P INTEGER,
    AREA_ALBERS_SQKM NUMERIC,
    geometry GEOMETRY,
    longitude NUMERIC,
    latitude NUMERIC
);


CREATE TABLE transport (
    name TEXT PRIMARY KEY,
    MB_CODE21 TEXT REFERENCES meshblock(MB_CODE21),
    feature_ty TEXT,
    additional TEXT,
    MB_CAT21 TEXT,
    SA1_CODE21 TEXT,
    SA2_CODE21 TEXT,
    SA2_NAME21 TEXT,
    AREASQKM21 NUMERIC,
    LOCI_URI21 TEXT,
    geometry GEOMETRY,
    longitude NUMERIC,
    latitude NUMERIC,
    operationa TEXT,  -- From gold_trains
    usage TEXT,        -- From gold_trains
    type TEXT
);

CREATE TABLE offences_agg (
    Type TEXT,
    monthyear DATE,
    MB_CODE21 TEXT REFERENCES meshblock(MB_CODE21),
    count INTEGER
);

CREATE TABLE offence (
    index INTEGER,
    Type TEXT,
    Date TIMESTAMP,
    Postcode TEXT,
    MB_CODE21 TEXT REFERENCES meshblock(MB_CODE21)
);

CREATE TABLE parks (
    OBJECTID INTEGER,
    PARK_NUMBE TEXT,
    PARK_NAME TEXT PRIMARY KEY,
    SAP_ASSET_ TEXT,
    HOUSE_NUMB TEXT,
    STREET_ADD TEXT,
    SUBURB TEXT,
    POST_CODE TEXT,
    longitude NUMERIC,
    latitude NUMERIC,
    SHAPE_Leng NUMERIC,
    SHAPE_Area NUMERIC,
    MEAN_X NUMERIC,
    MEAN_Y NUMERIC,
    MB_CODE21 TEXT REFERENCES meshblock(MB_CODE21),
    MB_CAT21 TEXT,
    AREASQKM21 NUMERIC,
    LOCI_URI21 TEXT,
    geometry GEOMETRY,
    centroid GEOMETRY
);

CREATE TABLE police (
    NAME TEXT PRIMARY KEY,
    MB_CODE21 TEXT REFERENCES meshblock(MB_CODE21),
    MB_CAT21 TEXT,
    geometry GEOMETRY,
    geometry_crs TEXT,
    centroid GEOMETRY,
    longitude NUMERIC,
    latitude NUMERIC,
    type TEXT
);

CREATE TABLE meshblock_intersect (
  mb_code21_from TEXT REFERENCES meshblock(MB_CODE21),
  mb_code21_to TEXT REFERENCES meshblock(MB_CODE21),
  type TEXT,
  PRIMARY KEY (mb_code21_from, mb_code21_to)
);

-- Create a table to store train station connections
CREATE TABLE train_station_connections (
    station_from TEXT REFERENCES transport(name),
    station_to TEXT REFERENCES transport(name),
    PRIMARY KEY (station_from, station_to)
);

ALTER TABLE meshblock ADD COLUMN centroid geometry(Point, 4326);
UPDATE meshblock 
SET centroid = ST_SetSRID(ST_MakePoint(longitude, latitude), 4326)

ALTER TABLE parks ADD COLUMN centroid geometry(Point, 4326);
UPDATE parks 
SET centroid = ST_SetSRID(ST_MakePoint(longitude, latitude), 4326)

ALTER TABLE police ADD COLUMN centroid geometry(Point, 4326);
UPDATE police 
SET centroid = ST_SetSRID(ST_MakePoint(longitude, latitude), 4326)


ALTER TABLE sa1 ADD COLUMN geometry geometry(MULTIPOLYGON, 4326);
UPDATE sa1 
SET geometry = ST_SetSRID(geometry_x, 4326)

CREATE INDEX meshblock_centroid_idx ON meshblock USING GIST (centroid);
CREATE INDEX park_centroid_idx ON parks USING GIST (centroid);
CREATE INDEX transport_centroid_idx ON transport USING GIST (centroid);
CREATE INDEX police_centroid_idx ON police USING GIST (centroid);



-- Import the data (replace 'path/to/file.csv' with the actual file paths)
COPY sa1 FROM 'C:\Users\Garyb\Desktop\thesis\neo4jcrime\scripts\gold\gold_sa1.csv' WITH (FORMAT CSV, HEADER);
-- COPY sa1 FROM 'C:\Users\Garyb\Desktop\thesis\neo4jcrime\scripts\gold\gold_gdfBris_sa1_go2.csv' WITH (FORMAT CSV, HEADER); -- No need to import twice
COPY meshblock FROM 'C:\Users\Garyb\Desktop\thesis\neo4jcrime\scripts\gold\gold_meshblocks.csv' WITH (FORMAT CSV, HEADER);
COPY offences_agg FROM 'C:\Users\Garyb\Desktop\thesis\neo4jcrime\scripts\gold\gold_offences_agg.tsv' WITH (FORMAT CSV, HEADER, DELIMITER E'\t'); 
COPY offence FROM 'C:\Users\Garyb\Desktop\thesis\neo4jcrime\scripts\gold\gold_offensives_from_2024-04-01_in_bris.csv' WITH (FORMAT CSV, HEADER);
COPY parks FROM 'C:\Users\Garyb\Desktop\thesis\neo4jcrime\scripts\gold\gold_parks.csv' WITH (FORMAT CSV, HEADER);
COPY transport FROM 'C:\Users\Garyb\Desktop\thesis\neo4jcrime\scripts\gold\gold_trains_busses.csv' WITH (FORMAT CSV, HEADER); -- Use the combined file
-- COPY transport FROM 'C:\Users\Garyb\Desktop\thesis\neo4jcrime\scripts\gold\gold_trains.csv' WITH (FORMAT CSV, HEADER); -- No need to import twice
COPY police FROM 'C:\Users\Garyb\Desktop\thesis\neo4jcrime\scripts\gold\gold_police.csv' WITH (FORMAT CSV, HEADER);
-- COPY police FROM 'C:\Users\Garyb\Desktop\thesis\neo4jcrime\scripts\gold\gold_police_stations.csv' WITH (FORMAT CSV, HEADER); -- No need to import twice






-- Insert neighbor relationships into the table


-- C:\Users\Garyb>"E:\Program Files\PostgreSQL\17\bin\pg_dump.exe" -h localhost -p 5432 -U postgres qldpolice2 > "C:\Users\Garyb\Downloads\bu_nov.sql"
-- Password:



  WITH RECURSIVE reachable_meshblocks AS (
  SELECT
    mb.MB_CODE21
  FROM meshblock AS mb
  JOIN transport AS p
    ON mb.MB_CODE21 = p.MB_CODE21
  
  UNION
  
  SELECT
    mb2.MB_CODE21
  FROM reachable_meshblocks
  JOIN meshblock_neighbors AS mn
    ON reachable_meshblocks.MB_CODE21 = mn.mb_code21_from
  JOIN meshblock AS mb2
    ON mn.mb_code21_to = mb2.MB_CODE21
)
SELECT

  AVG(cs.count) AS average_crime_count
FROM reachable_meshblocks
JOIN offences_agg AS cs
  ON reachable_meshblocks.MB_CODE21 = cs.MB_CODE21