
### SA1  
```SQL
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_sa1.csv' AS row
MERGE (a:SA1 {SA1_CODE_2021: row.SA1_CODE_2021})
SET a += row
```

### Meshblocks
```SQL
CREATE CONSTRAINT UniqueMeshblock FOR (mb:MeshBlock) REQUIRE mb.MB_CODE21 IS UNIQUE
```
```SQL 
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_meshblocks.csv' AS row
MERGE (a:MeshBlock {MB_CODE21: row.MB_CODE21})
SET a += row
```

## Pre calculated near relationship
```sql
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_meshblock_relationships.csv' AS row
MERGE (mb1:MeshBlock {MB_CODE21: row.mb_code21_from})
MERGE (mb2:MeshBlock {MB_CODE21: row.mb_code21_to})
MERGE (mb1)-[:INTERSECTS {type: 'meshblock'}]->(mb2)
```

### Police stations
```SQL
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_police.csv' AS row
MERGE (a:PoliceStation {NAME: row.NAME})
SET a += row
```

### Trains
```SQL
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_trains_busses.csv' AS row
MERGE (a:Transport {name: row.name})
SET a += row
```

```SQL
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_train_to_train.csv' AS row
MERGE (a:Transport {name: row.TrainStationFrom})
MERGE (b:Transport {name: row.TrainStationTo})
MERGE (a)-[:GOES_TO]->(b)
```
	TrainStationTo


### Parks
```SQL
CREATE CONSTRAINT UniquePark FOR (mb:Park) REQUIRE mb.PARK_NAME IS UNIQUE
```
```SQL
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_parks.csv' AS row
MERGE (a:Park {PARK_NAME: row.PARK_NAME})
SET a += row
```

### Offence aggregated 
```SQL
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_offences_agg.csv' AS row
MERGE (a:OffencesAgg { 
    count: toInteger(row.count),
    date: row.monthyear, 
    type: row.Type,
    MB_CODE21: row.MB_CODE21
})
SET a += row
```


### Offence  
```
LOAD CSV WITH HEADERS FROM 'https://gaz-knowledge-graph.s3.ap-southeast-2.amazonaws.com/gold_offensives_from_2024-04-01_in_bris.csv' as offence
MATCH (mb:MeshBlock { MB_CODE21: offence.MB_CODE21}) 
CREATE (of:Offence { 
    date: offence.Date, 
    type: offence.Type,
    MB_CODE21: offence.MB_CODE21
})
CREATE (of)-[:LOCATED_IN]->(mb) 
<!-- ```
Note:
## You can import the offences as csv or use the python script. Since they are quite many this process 
## will take sometime 
## import the offences
LOAD CSV WITH HEADERS FROM 'file:///offence.csv' AS row
MERGE (a:Offences {OffenceID: row.index})
SET a += row -->
